#!/usr/bin/env python

# N Queens Problem
# This simple solution uses recursive backtracking to perform
# an exhaustive search.  It is not optimized in any way.
#
# Run like this from the command line for a 5x5 board:
#   python nqueens.py 5
# or without a parameter for a default board of size 4

import numpy as np
import sys

def boardConfigurationIsValid(b):
    """Returns true if the board contains a valid configuration of Queens
        Not the most efficient way, but relatively easy to do.
    """
    # assumes queens are ones and empty spots are zeros
    # each row or column must sum to 0 or 1 (allow for partial boards)
    if np.any(b.sum(axis=0) > 1) or np.any(b.sum(axis=1) > 1):
        return False
    # No two or more queens on diagonals
    # numpy trace is a sum along diagonals
    n = np.shape(b)[0]      # get the dimension, since we didn't pass it in
    bp = np.fliplr(b)       # flip it to check the other diagonals
    # offsets from main diagonal to check
    for i in range(-(n-2),n-1):
        if np.trace(b,offset=i) > 1 or np.trace(bp,offset=i) > 1:
            return False
    return True       
        
def placeQueen(n,nmax,col,board,sols):
    """Recursive backtracking call to try placing n queens starting in 
       column col, using board.  When a solution is found, makes a copy
       of the board and places it into sols.
        n   = number of queens yet to be placed
        nmax= board size
        col = column to try placing into
        board = a numpy array of ints representing the board, queens are ones
        sols  = a list of boards that are solutions
    """
    for row in range(nmax):
        # place queen (in next row)
        board[row,col] = 1
        if boardConfigurationIsValid(board):
            if col == nmax-1:
                sols.append(np.copy(board))
            else:
                placeQueen(n-1, nmax, col+1, board, sols)
        # pick up queen (backtrack)
        board[row,col] = 0

def main(argv):
    n = 4
    # expect board size in first argument
    if len(argv) >= 1:
        n = int(argv[0])

    print("Finding solutions to the {}x{} Queens Problem ...".format(n,n))

    # Set up our board.  Numpy array of size (n,n) of type int
    board = np.zeros((n,n), dtype=int)

    # Begin search
    solutions = []
    placeQueen(n,n,0,board,solutions)
    
    print("There are {} solutions".format(len(solutions)))
    if len(solutions) > 0 and len(solutions) < 12:
        print(*solutions, sep="\n")       #Python 3 version of print


if __name__ == "__main__":
    main(sys.argv[1:])